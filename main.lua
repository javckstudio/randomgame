-----------------------------------------------------------------------------------------
--
-- main.lua
-- 本程式示範如何產生隨機的數字
-- Author: Zack Lin
-- Time: 2015/4/1
-----------------------------------------------------------------------------------------

-- Your code here
_SCREEN = {
	WIDTH = display.viewableContentWidth,
	HEIGHT = display.viewableContentHeight
}

_SCREEN.CENTER = {
	X = display.contentCenterX,
	Y = display.contentCenterY
}
display.setStatusBar( display.HiddenStatusBar)
local widget = require( "widget" )

--==================================================================================
--宣告function
--==================================================================================
local yourFortune
local canReceiveShakeEvent = true

local hideFortune = function ()
	yourFortune.isVisible = false
	canReceiveShakeEvent = true
end

--每支程式要開始使用亂數之前，要先使用下列指令來打亂亂數表，否則都使用相同的亂數表
math.randomseed( os.time() )
local tellMeSomething = function ()
	--取得一個介於1~6之間的隨機整數
	local fortuneNumber = math.random( 6 )
	tempFortune = display.newImageRect( fortuneNumber ..".png" , 410, 232 )
	yourFortune[1]:removeSelf( )
	yourFortune:insert( tempFortune)
	yourFortune.isVisible = true
end

local magicSound = audio.loadSound( "Magic.mp3" )
local buttonPressed = function ( event )
	if("began" == event.phase) then
		audio.play( magicSound )
		yourFortune.isVisible = false
	elseif("ended" == event.phase) then
		tellMeSomething()
	end
end

local function shakeListener( event )
	if event.isShake  and canReceiveShakeEvent then
		--print( "device shake!" )
		canReceiveShakeEvent = false
		tellMeSomething()
		timer.performWithDelay( 3000 , hideFortune , 1 )
	end
	return true
end
--==================================================================================
--顯示物件
--==================================================================================
local background = display.newImageRect( "Background.png" , _SCREEN.WIDTH, _SCREEN.HEIGHT )
background.x = _SCREEN.CENTER.X
background.y = _SCREEN.CENTER.Y
local decoWords = display.newImageRect( "Words.png", _SCREEN.WIDTH, 420 )
decoWords.x = 319
decoWords.y = 367

local oldMan = display.newImageRect(  "OldMan.png", _SCREEN.WIDTH, _SCREEN.HEIGHT )
oldMan.x = _SCREEN.CENTER.X
oldMan.y = _SCREEN.CENTER.Y

local truthButton = widget.newButton{
	width = 391,
	height = 89,
	defaultFile = "TellButton.png",
	overFile = "TellButtonPressed.png",
	onEvent = buttonPressed
}
truthButton.x = _SCREEN.CENTER.X
truthButton.y = _SCREEN.HEIGHT - 65

yourFortune = display.newGroup( )
local tempFortune = display.newImageRect( "1.png" , 410 , 232 )
yourFortune:insert( tempFortune )
yourFortune.x = _SCREEN.CENTER.X
yourFortune.y = _SCREEN.HEIGHT - 278
yourFortune.isVisible = false

--==================================================================================
--加入偵聽器
--==================================================================================
Runtime:addEventListener( "accelerometer", shakeListener )